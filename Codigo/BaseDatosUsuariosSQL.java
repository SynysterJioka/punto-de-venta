/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopuntodeventa;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author OO
 */
public class BaseDatosUsuariosSQL {
    
     //_____________________CREAR BASE DE DATOS__________________________________
    public void createNewDatabase(String baseDatos) {
 
        String url = "jdbc:sqlite:" + baseDatos;
 
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Una nueva base de datos ha sido creada.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    //__________Crear tabla donde se almanecaran los datos_________
    
    public void createNewTable(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Usuarios_registrados (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	nombre text NOT NULL,\n"
                + "	paterno text NOT NULL,\n"
                + "     materno text NOT NULL,\n"
                + "     correo text NOT NULL,\n"
                + "     fechaNacimiento text NOT NULL,\n"
                + "     NombreDeUsuario text NOT NULL,\n"
                + "     direccion text NOT NULL,\n"
                + "     telefono text NOT NULL,\n"
                + "     contrasenia text NOT NULL,\n"
                + "     IDE text NOT NULL\n"
                + ");";
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            System.out.println("tabla creada");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }  
    
    
    
    
    
    //Conectar a la base de datos
    private Connection connect(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    
    
    
    
    public void insert(String baseDatos, String[] datos) {
        String sql = "INSERT INTO Usuarios_registrados (nombre, paterno, materno,"
                + " correo, fechaNacimiento, NombreDeUsuario, direccion, "
                + "telefono, contrasenia, IDE) VALUES(?,?,?,?,?,?,?,?,?,?)";
 
        try (Connection conn = this.connect(baseDatos);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, datos[0]);
            pstmt.setString(2, datos[1]);
            pstmt.setString(3, datos[2]);
            pstmt.setString(4, datos[3]);
            pstmt.setString(5, datos[4]);
            pstmt.setString(6, datos[5]);
            pstmt.setString(7, datos[6]);
            pstmt.setString(8, datos[7]);
            pstmt.setString(9, datos[8]);
            pstmt.setString(10, datos[9]);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    
    public void selectAll(String baseDatos){
        String sql = "SELECT id,nombre, paterno, materno, correo, "
                + "fechaNacimiento, NombreDeUsuario, direccion, telefono,"
                + "contrasenia, IDE FROM  Usuarios_registrados ";  //(where id = 1)   <-- va despues de Usuario 
                                                                                                                                                                                    // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" + 
                                   rs.getString("nombre") + "\t" +
                                   rs.getString("paterno") + "\t" +
                                   rs.getString("materno") + "\t" +
                                   rs.getString("correo") + "\t" +
                                   rs.getString("fechaNacimiento") + "\t" +
                                   rs.getString("NombreDeUsuario") + "\t" +                                   
                                   rs.getString("direccion") + "\t" +
                                   rs.getString("telefono")+ "\t" +
                                   rs.getString("contrasenia") + "\t" +
                                   rs.getString("IDE")); 
            }
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    
}

    

