/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopuntodeventa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author OO
 */
public class Pruebas {
    
    
    //Conectar a la base de datos
    private Connection connect(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    
    
    
    
    //Acceso
     public boolean Acceso(String baseDatos, String nombreUsuario, String contrasenia){
         boolean acceso=false;
        String sql = "SELECT id,NombreDeUsuario, contrasenia FROM Usuarios_registrados ";  //(where id = 1)   <-- va despues de Usuario 
                                                                                                                                                                                    // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
                        
            while (rs.next()) {                               
                if((rs.getString("NombreDeUsuario").equals(nombreUsuario))
                        &&(rs.getString("contrasenia").equals(contrasenia))){
                     acceso=true;
                     return acceso;
                     
                }                                 
             }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("No se armó");
        }
        return acceso;                    
    } 
     
     
     public void MostrarEnTabla(String baseDatos, DefaultTableModel md){
         
        String sql = "SELECT id,nombre, paterno, materno, correo, fechaNacimiento,"
                + " NombreDeUsuario, direccion, telefono, IDE FROM  Usuarios_registrados ";  //(where id = 1)   <-- va despues de Usuario 
                                                                                                                                                                                    // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
                        
            while (rs.next()) {                               
                String datos[]={rs.getString("id"),rs.getString("nombre"),
                rs.getString("paterno"), rs.getString("materno"),
                rs.getString("correo"), rs.getString("telefono"),
                rs.getString("direccion"),rs.getString("fechaNacimiento"),
                rs.getString("NombreDeUsuario"),rs.getString("IDE")}; 
                md.addRow(datos);
             }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("No se armó");
        }
                            
    } 
     
     
     public int TamanioTabla(String baseDatos){
         
        String sql = "SELECT id FROM Usuarios_registrados ";  //(where id = 1)   <-- va despues de Usuario 
              int tam=0;                                                                                                                                                                      // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
                        
            while (rs.next()) {                               
                tam++;
             }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("No se armó");
        }return tam;
                            
    } 
     
     
      public void MostrarTablaSencillaUsuarios(String baseDatos, DefaultTableModel md){
         
        String sql = "SELECT id,nombre, paterno, materno FROM  Usuarios_registrados ";  //(where id = 1)   <-- va despues de Usuario 
                                                                                                                                                                                    // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
                        
            while (rs.next()) {                               
                String datos[]={rs.getString("id"),rs.getString("nombre"),
                rs.getString("paterno"), rs.getString("materno")};
                 
                md.addRow(datos);
             }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.out.println("No se armó");
        }
                            
    } 
     
      
      
       public void TextoSinNumeros(java.awt.event.KeyEvent evt){
        char texto= evt.getKeyChar();
        if((texto < 'a' || texto>'z')&& (texto<'A'|| texto>'Z')){
            evt.consume();
    }
    }
    
    public void TextoSoloDigitos(java.awt.event.KeyEvent evt){
         char digito= evt.getKeyChar();
        if( digito<'0' || digito>'9'){
            evt.consume();
    }
    }
    
    
    
     public void EditarFila(String baseDatos, String[] datos, int fila) {
        String sql = "UPDATE Usuarios_registrados SET "
                + "nombre=?,"
                + "paterno=?,"
                + "materno=?,"
                + "correo=?,"
                + "fechaNacimiento=?,"
                + "NombreDeUsuario=?,"
                + "direccion=?, "
                + "telefono=?,"
                + "contrasenia=?"
                + " where id="+fila;
 
        try (Connection conn = this.connect(baseDatos);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, datos[0]);
            pstmt.setString(2, datos[1]);
            pstmt.setString(3, datos[2]);
            pstmt.setString(4, datos[3]);
            pstmt.setString(5, datos[4]);
            pstmt.setString(6, datos[5]);
            pstmt.setString(7, datos[6]);
            pstmt.setString(8, datos[7]);
            pstmt.setString(9, datos[8]);
            
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
     
     
     
     public void EliminarFila(String baseDatos, String[] datos, int fila) {
        String sql = "DELETE FROM Usuarios_registrados WHERE nombre=?";
                
                
 
        try (Connection conn = this.connect(baseDatos);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, datos[0]);
           
            
            
            
            
            int rowsDeleted = pstmt.executeUpdate();
            if (rowsDeleted > 0) {
                
        System.out.println("Usuario eliminado");
            }
            
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
      public String[] RegresarDatoAUnVector(String baseDatos, int fila){
        String sql = "SELECT id,nombre, paterno, materno, correo, fechaNacimiento,"
                + " NombreDeUsuario, direccion, telefono, contrasenia, IDE FROM"
                + "  Usuarios_registrados where id ="+fila;
        String[]dato=new String[10];
                                                                                                                                                                                     // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            
                
                dato[0]=rs.getString("nombre");
                dato[1]=rs.getString("paterno");
                dato[2]=rs.getString("materno");
                dato[3]=rs.getString("correo");
                dato[4]=rs.getString("fechaNacimiento");
                dato[5]=rs.getString("NombreDeUsuario");
                dato[6]=rs.getString("direccion");
                dato[7]=rs.getString("telefono");
                dato[8]=rs.getString("contrasenia");
                dato[9]=rs.getString("IDE");
                                
                                   
            
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }return dato;
    }
    
}
