/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopuntodeventa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author OO
 */
public class Productos extends javax.swing.JFrame {

    DefaultTableModel Tmd;
    String data[][]={};
    String cabeza[]={"#","Nombre del Producto", "Cantidad en Inventario", "Precio"};
    
    DefaultComboBoxModel cmd;
    String productosLista[];
    
    public Productos() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        Tmd=new DefaultTableModel( data, cabeza);
        TblProductos.setModel(Tmd);
        MFP.MostrarEnTabla("Productos_En_Inventario", Tmd);
        
       
        CmbBoxTiposProductos.addItem("Selecciones el producto a agregar");
        this.AgregarItemComboBox("Productos_En_Inventario");
           
        
     /*cmd=new DefaultComboBoxModel(productosLista);
        int tamanio=MFP.TamanioTabla("Productos_En_Inventario");
        String productosEnInventario[]=MFP.RegresarDatoAUnVectorProductos
        ("Productos_En_Inventario", tamanio, cmd );*/
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        TblProductos = new javax.swing.JTable();
        BttnRegresar = new javax.swing.JButton();
        CmbBoxTiposProductos = new javax.swing.JComboBox<>();
        SpnnrCantidad = new javax.swing.JSpinner();
        TxtFldPrecio = new javax.swing.JTextField();
        BttnNuevoP = new javax.swing.JButton();
        SpnnrSumar = new javax.swing.JSpinner();
        LblCantidad = new javax.swing.JLabel();
        LblPrecio = new javax.swing.JLabel();
        TxtFldNombreP = new javax.swing.JTextField();
        LblNomProducto = new javax.swing.JLabel();
        BttnAgregar = new javax.swing.JButton();
        LblFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TblProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(TblProductos);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 30, 550, 90));

        BttnRegresar.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        BttnRegresar.setText("Regresar");
        BttnRegresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BttnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BttnRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(BttnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 400, -1, -1));

        CmbBoxTiposProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CmbBoxTiposProductosActionPerformed(evt);
            }
        });
        getContentPane().add(CmbBoxTiposProductos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 160, -1));

        SpnnrCantidad.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        getContentPane().add(SpnnrCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 290, 130, 30));

        TxtFldPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtFldPrecioActionPerformed(evt);
            }
        });
        getContentPane().add(TxtFldPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 330, 130, 30));

        BttnNuevoP.setText("Nuevo Producto");
        BttnNuevoP.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BttnNuevoP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BttnNuevoPActionPerformed(evt);
            }
        });
        getContentPane().add(BttnNuevoP, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 380, -1, -1));

        SpnnrSumar.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        getContentPane().add(SpnnrSumar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 330, 80, 30));

        LblCantidad.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        LblCantidad.setForeground(new java.awt.Color(0, 0, 0));
        LblCantidad.setText("Cantidad:");
        getContentPane().add(LblCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 300, -1, -1));

        LblPrecio.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        LblPrecio.setForeground(new java.awt.Color(0, 0, 0));
        LblPrecio.setText("Precio:");
        getContentPane().add(LblPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, -1, -1));
        getContentPane().add(TxtFldNombreP, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 250, 130, 30));

        LblNomProducto.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        LblNomProducto.setForeground(new java.awt.Color(0, 0, 0));
        LblNomProducto.setText("<html>\n<p>Nombre del</p>\n<p>roducto:</p>");
        getContentPane().add(LblNomProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, -1, -1));

        BttnAgregar.setText("Agregar");
        BttnAgregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BttnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BttnAgregarActionPerformed(evt);
            }
        });
        getContentPane().add(BttnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 370, 80, -1));

        LblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FONDITOO.jpeg"))); // NOI18N
        getContentPane().add(LblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 440));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BttnNuevoPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BttnNuevoPActionPerformed
        String nombreBD="Productos_En_Inventario";
        String[] datos=new String[3];
        datos[0]=TxtFldNombreP.getText().toLowerCase();
        datos[1]=SpnnrCantidad.getValue().toString();
        datos[2]=TxtFldPrecio.getText();
        
        BDPsql.createNewDatabase("nombreBD");
        BDPsql.createNewTable(nombreBD);
        BDPsql.insert(nombreBD, datos);
        
        this.setVisible(true);
    }//GEN-LAST:event_BttnNuevoPActionPerformed

    private void BttnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BttnRegresarActionPerformed
       regresar.setVisible(true);
       this.setVisible(false);
    }//GEN-LAST:event_BttnRegresarActionPerformed

    private void CmbBoxTiposProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CmbBoxTiposProductosActionPerformed
        //CmbBoxTiposProductos.
    }//GEN-LAST:event_CmbBoxTiposProductosActionPerformed

    private void BttnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BttnAgregarActionPerformed
        String s_suma=SpnnrSumar.getValue().toString();
        int suma=Integer.parseInt(s_suma);
        
        String s_Producto=CmbBoxTiposProductos.getSelectedItem().toString().toLowerCase();
        int fila=MFP.BuscarProducto("Productos_En_Inventario", s_Producto);
        
        
        String datos[]=MFP.RegresarDatoAUnVector("Productos_En_Inventario", fila);
        
        
        
        if(fila!=0){
            MFP.AgregarSoloProductos("Productos_En_Inventario", datos, fila, suma);
            
        }
        actualizarVentana();
    }//GEN-LAST:event_BttnAgregarActionPerformed

    private void TxtFldPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtFldPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtFldPrecioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Productos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BttnAgregar;
    private javax.swing.JButton BttnNuevoP;
    private javax.swing.JButton BttnRegresar;
    private javax.swing.JComboBox<String> CmbBoxTiposProductos;
    private javax.swing.JLabel LblCantidad;
    private javax.swing.JLabel LblFondo;
    private javax.swing.JLabel LblNomProducto;
    private javax.swing.JLabel LblPrecio;
    private javax.swing.JSpinner SpnnrCantidad;
    private javax.swing.JSpinner SpnnrSumar;
    private javax.swing.JTable TblProductos;
    private javax.swing.JTextField TxtFldNombreP;
    private javax.swing.JTextField TxtFldPrecio;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
BaseDatoProductosSQL BDPsql=new BaseDatoProductosSQL();
MetodosFuncionesProductos MFP= new MetodosFuncionesProductos();
PanelSupervisor regresar=new PanelSupervisor();





//Conectar a la base de datos
    private Connection connect(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }





public String AgregarItemComboBox(String baseDatos){
        String sql = "SELECT nombreP FROM"
                + "  Productos_En_Inventario";
        String datos=" ";
        int o=0;
                                                                                                                                                                                     // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
             while (rs.next()) {  
             CmbBoxTiposProductos.addItem(rs.getString("nombreP"));
             
             
             }
                
                
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }return datos;
    }
private void actualizarVentana(){
    SwingUtilities.updateComponentTreeUI(this);
}
}
