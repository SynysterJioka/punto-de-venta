/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopuntodeventa;
import javax.swing.JOptionPane;
/**
 *
 * @author OO
 */
public class InicioSesionUsuario extends javax.swing.JFrame {

    /**
     * Creates new form InicioSesionUsuario
     */
    public InicioSesionUsuario() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        bttnSalir = new javax.swing.JButton();
        lblIconUsuario = new javax.swing.JLabel();
        PsswrdFldContrasenia = new javax.swing.JPasswordField();
        bttnIniciar = new javax.swing.JButton();
        bttnSupervisor = new javax.swing.JButton();
        TxtFldNombreUsuario = new javax.swing.JTextField();
        lblNombreDeUsuario = new javax.swing.JLabel();
        lblContrasenia = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1040, 780));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bttnSalir.setText("Salir");
        bttnSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bttnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(bttnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 20, -1, -1));

        lblIconUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Usuario.png"))); // NOI18N
        getContentPane().add(lblIconUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 0, -1, -1));

        PsswrdFldContrasenia.setBackground(new java.awt.Color(255, 255, 255));
        PsswrdFldContrasenia.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        PsswrdFldContrasenia.setForeground(new java.awt.Color(0, 0, 0));
        PsswrdFldContrasenia.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        getContentPane().add(PsswrdFldContrasenia, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 280, 150, -1));

        bttnIniciar.setText("Iniciar");
        bttnIniciar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bttnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnIniciarActionPerformed(evt);
            }
        });
        getContentPane().add(bttnIniciar, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 320, -1, -1));

        bttnSupervisor.setText("Supervisor");
        bttnSupervisor.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bttnSupervisor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bttnSupervisorActionPerformed(evt);
            }
        });
        getContentPane().add(bttnSupervisor, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 390, -1, -1));

        TxtFldNombreUsuario.setBackground(new java.awt.Color(255, 255, 255));
        TxtFldNombreUsuario.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        TxtFldNombreUsuario.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        getContentPane().add(TxtFldNombreUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, 150, -1));

        lblNombreDeUsuario.setBackground(new java.awt.Color(255, 255, 255));
        lblNombreDeUsuario.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        lblNombreDeUsuario.setForeground(new java.awt.Color(0, 0, 0));
        lblNombreDeUsuario.setText("Nombre de usuario: ");
        lblNombreDeUsuario.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        getContentPane().add(lblNombreDeUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 240, 150, -1));

        lblContrasenia.setBackground(new java.awt.Color(255, 255, 255));
        lblContrasenia.setFont(new java.awt.Font("Gill Sans MT", 0, 12)); // NOI18N
        lblContrasenia.setForeground(new java.awt.Color(0, 0, 0));
        lblContrasenia.setText("Contraseña: ");
        lblContrasenia.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        getContentPane().add(lblContrasenia, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 280, 150, -1));

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo1.jpeg"))); // NOI18N
        getContentPane().add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 430));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bttnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_bttnSalirActionPerformed

    private void bttnSupervisorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnSupervisorActionPerformed
        // TODO add your handling code here:
        InicioSesionSupervisor abrir= new InicioSesionSupervisor();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_bttnSupervisorActionPerformed

    private void bttnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bttnIniciarActionPerformed
        // TODO add your handling code here:
        
        String nombreUsuario=TxtFldNombreUsuario.getText().toLowerCase();
        String contrasenia=PsswrdFldContrasenia.getText();
        
        boolean acceso=base.Acceso("Usuarios_registrados", nombreUsuario, contrasenia);
        if(acceso==false){
            JOptionPane.showMessageDialog(null, "Error al poner nombre de usuario o contraseña");
            TxtFldNombreUsuario.setText("");
            PsswrdFldContrasenia.setText("");
        }else{
            System.out.println("Funciona");
            
            Venta acceder= new Venta();
            acceder.setVisible(true);
            this.setVisible(false);
            
        }
        
        
    }//GEN-LAST:event_bttnIniciarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioSesionUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioSesionUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioSesionUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioSesionUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioSesionUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField PsswrdFldContrasenia;
    private javax.swing.JTextField TxtFldNombreUsuario;
    private javax.swing.JButton bttnIniciar;
    private javax.swing.JButton bttnSalir;
    private javax.swing.JButton bttnSupervisor;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblContrasenia;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblIconUsuario;
    private javax.swing.JLabel lblNombreDeUsuario;
    // End of variables declaration//GEN-END:variables
private Pruebas base=new Pruebas();
}
