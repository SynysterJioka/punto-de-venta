/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectopuntodeventa;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author OO
 */
public class BaseDatoProductosSQL {
     public void createNewDatabase(String baseDatos) {
 
        String url = "jdbc:sqlite:" + baseDatos;
 
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Una nueva base de datos ha sido creada.");
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    //__________Crear tabla donde se almanecaran los datos_________
    
    public void createNewTable(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS Productos_En_Inventario (\n"
                + "	id integer PRIMARY KEY,\n"
                + "	nombreP text NOT NULL,\n"
                + "	cantidad text NOT NULL,\n"
                + "     precio text NOT NULL\n"
                + ");";
                
        
        try (Connection conn = DriverManager.getConnection(url);
                Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            System.out.println("tabla creada");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }  
    
    
    
    
    
    //Conectar a la base de datos
    private Connection connect(String baseDatos) {
        // SQLite connection string
        String url = "jdbc:sqlite: "+baseDatos;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
    
    
    
    
    
    public void insert(String baseDatos, String[] datos) {
        String sql = "INSERT INTO Productos_En_Inventario (nombreP, cantidad,"
                + " precio) VALUES(?,?,?)";
 
        try (Connection conn = this.connect(baseDatos);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, datos[0]);
            pstmt.setString(2, datos[1]);
            pstmt.setString(3, datos[2]);
            
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    
    
    public void selectAll(String baseDatos){
        String sql = "SELECT id,nombreP, cantidad,"
                + " precio FROM  Productos_En_Inventario ";  //(where id = 1)   <-- va despues de Usuario 
                                                                                                                                                                                    // tel prof 618-127-2214
        
        try (Connection conn = this.connect(baseDatos);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") +  "\t" + 
                                   rs.getString("nombreP") + "\t" +
                                   rs.getString("cantidad") + "\t" +
                                   rs.getString("precio")); 
            }
            
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
