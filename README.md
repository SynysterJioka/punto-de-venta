Alan Amador Flores Fiscal.

Gabriela de Jesús Vargas Leos.

Jose Manuel Aguilar Luna. 


TEORÍA DE COLOR; SELECCIÓN DE DISEÑO:

*Blanco. - fue elegido por su amplia relación con el profesionalismo, así como
expresar paz. Con lo que se busca que quien utilice el programa se sienta relajado.

*Naranja. -  color secundario que posee un carácter acogedor y cálido; buscando 
una cualidad dinámica y enérgica en quién lo observe. 

*Verde. - utilizado principalmente en los botones relacionados con la confirmación
de alguna acción. Al ser caracterizado universalmente como aprobación. (claro
ejemplo es en los semáforos).

*Rojo. - colocado mayormente en los botones relacionados con cancelar una acción,
por su amplia relación con la restricción.

*Colores cálidos. - para buscar un efecto confortable y evitar el estrés.


